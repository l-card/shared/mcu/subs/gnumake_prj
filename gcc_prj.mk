################################################################################
#   Файл для сборки проектов с помощью gnu-make и gcc.
#   В основном makefile проекта устанавливаются необходимые переменные, после
#   чего включается данный файл с помощью
#
#   include <путь>/gcc_prj.mk
#
#   После включения также надо добавить зависимость цели сборки проекта от
#   результирующего файла (по умолчанию устанавливается зависимость от .elf).
#   Также возможно добавление зависимостей от своих целей.
#   Например, если нужно, чтобы при сборке проекта был создан .bin и .hex файл,
#   то после включения данного файла нужно добавить:
#
#   $(PROJECT_TARGET): $(PROJECT_BIN) $(PROJECT_HEX)
#
# Реализованы следующие возможности:
#   - создает цели для компиляции файлов C, C++ и ASM с линковкой в .elf и
#     генерацией .bin,.hex и/или .ldr
#   - автоматическое определение типа исходника (C, C++ или ASM) по расширению
#     файла.
#   - автоматическое отслеживание зависимостей файлов
#   - все результаты компиляции складываются в отдельный каталог $(PROJECT_BUILD_DIR)
#   - создание всех необходимых директорий для сборки
#   - генерация .ld файла из ld.S путем прохода препроцессора gcc
#   - возможность сборки разных групп исходных кодов с разными флагами
#
#
# Используемые утилиты:
#              (CC, CXX, AS, SIZE, HEX, BIN по умолчанию используются для сборки с GCC
#               с возможностью определить префикс через TOOLCHAIN_PREFIX)
#
#   MKDIR    - команда для создания директории (по умолчанию mkdir -p)
#   RMDIR    - команда для удаления файлов/директории (по умолчанию rm -rf)
#   CC       - команда для компиляции С файлов (она же для линковки .elf без исходников на C++)
#   CXX      - команда для компиляции С++ файлов (она же для линковки .elf с исходниками на C++)
#   AS       - команда для компиляции ASM-файлов
#   SIZE     - команда для отображения информации о .elf файле (размер и т.п.)
#   HEX      - команда для создания .hex из .elf (если требуется)
#   BIN      - команда для создания .bin из .elf (если требуется)
#   LDR      - команда для создания .ldr из .elf (для bfin, если требуется)
#
#
# Параметры проекта:
#   PROJECT_NAME            - назвение проекта (определяет название выходного файла)
#   PROJECT_SOURCE_DIR      - абсолютный путь к корню проекта - все файлы исходных
#                               кодов должны быть внутри данного каталога
#                               (по умолчанию используется директория верхнего makefile)
#   PROJECT_BUILD_DIR       - директория, куда будут сохранены все результаты сборки.
#                               (по умолчанию используется подкаталог
#                                $(PROJECT_BUILD_ROOT_DIR) или
#                                $(PROJECT_BUILD_ROOT_DIR)/$(PROJECT_BUILD_TYPE),
#                                если задан PROJECT_BUILD_TYPE).
#   PROJECT_BUILD_ROOT_DIR  - корневая директория, куда будут сохранены реузультаты
#                                сборки. По умолчанию использется директория builds.
#                                Если не задана $(PROJECT_BUILD_TYPE), то эта
#                                директория соответствует $(PROJECT_BUILD_DIR),
#                                иначе - как корневая директория в которой под
#                                каждый тип сборки создается своя поддиректория сборки
#   PROJECT_BUILD_TYPE      - вариант сборки проекта. Если задан, то влияет на
#                                диеркторию сборки по умолчанию (сборка выполняется
#                                в директории с именем $(PROJECT_BUILD_TYPE)
#                                в корневой директории сборки $(PROJECT_BUILD_ROOT_DIR)
#   PROJECT_TARGET          - цель для сборки проекта (по умолчанию all)
#   PROJECT_TARGET_CLEAN    - цель для очистки проекта (по умолчанию clean)
#
#  Параметры сборки:
#   SRC                     - список исходных кодов, если не используются группы.
#                             тип различается по расширению:
#                              - С - .c
#                              - C++ - .cpp, .cxx, .cc
#                              - ASM - .s, .S
#   DEFS                    - набор определений компилятора (обычный список <определение>
#                                   или <определение>=<значение>)
#   INC_DIRS                - список директорий для поиска заголовков
#   SYS_INC_DIRS            - список директорий для поиска системных заголовков
#                             (не применяются проверки на warnings)
#   LIB_DIRS                - список директорий для поиска библиотек
#   LIBS                    - список библиотек для линковки
#   LDSCRIPT_TEMPLATE       - файл линкера с расширением .ld.S, который может быть
#                             обработан препроцессором (для подстановки определений
#                             из $(LD_DEFS) и включения других файлов с использованием
#                             путей заголовков $(LD_INC_DIRS) и $(LD_SYS_INC_DIRS)).
#                             Если не определен, то сборка идет без явного указания
#                             ld-файла.
#   CFLAGS                  - флаги для компиляции C-файлов (без флагов для генерации
#                             зависимостей, .lst и других, добавляемых фреймворком).
#                             Достаточно указания флагов, специфичных для контроллера,
#                             и оптимизации (при желании также используемый стандарт C)
#   CPPFLAGS                - флаги для компиляции C++ файлов (без флагов для генерации
#                             зависимостей, .lst и других, добавляемых фреймворком).
#   ASFLAGS                 - флаги для компиляции файлов ассемблера
#   LDFLAGS                 - флаги, используемые при линковки (без флагов генерации
#                             map-файла и подстановки ld-скрипта)
#   LDRFLAGS                - общие флаги для генерации ldr из .elf, если нужны
#   LDRCREATEFLAGS          - флаги для создания ldr из .elf, специфичные для операции
#                             create для доп. параметров ldr-файла
#   LDRDEPS                 - дополнительные зависимости при создании .ldr-файла
#   LDREXTRAELFS            - дополнительные .elf файлы для создания .ldr-файла
#   LDREXTRATARGETS         - дополнительная цель, служащая для генерации .elf из
#                             LDREXTRAELFS
#   LD_DEFS                 - определения, используемые при обработке .ld.S файла
#                             препроцессором. По умолчанию эквивалентно DEFS.
#   LD_INC_DIRS             - список директорий для заголовков при обработке .ld.S файла
#                             препроцессором. По умолчанию эквивалентно INC_DIRS.
#   LD_SYS_INC_DIRS         - список директорий для поиска системных заголовков
#                             при обработке .ld.S файла препроцессором.
#                             По умолчанию эквивалентно SYS_INC_DIRS.
#   WARNOPTS                - может использоваться для переопределения опций для
#                             задания уровня предупреждений (если нужны отличные,
#                             от используемых по умолчанию)
#   PROJECT_ELF_FILENAME    - задание результирующего исполняемого файла (без директории).
#                             По умолчанию ${PROJECT_NAME}.elf
#   PROJECT_PREBUILD_DEPS   - зависимости сборки (цели которые должны быть выполнены
#                             перед сборкой - от которых зависит компиляция исходников)
#   BUILD_GROUPS            - переменная определяет набор групп для сборки
#                             исходных кодов. Если не задана (или задана пустой),
#                             то используется одна группа с файлами из
#                             $(SRC). Если же данная переменная
#                             задана, то сборка идет по группам из файлов
#                             $(SRC_$(GROUP)), где
#                             $(GROUP) - слово из BUILD_GROUPS (название группы).
#                             При этом для сборки файлов группы помимо переменных
#                             без префикса (которые применяются ко всем группам)
#                             используются переменные с префиксом _$(GROUP):
#                               - DEFS_$(GROUP)
#                               - INC_DIRS_$(GROUP)
#                               - SYS_INC_DIRS_$(GROUP)
#                               - CFLAGS_$(GROUP)
#                               - ASFLAGS_$(GROUP)
#  EXTRA_GROUPS             - дополнительные группы, для которых производится
#                             только создания объектных файлов без использования
#                             их в проекте
#  BFIN_USE_INITCODE        - опция для процессора blackfin для использования
#                             init-кода. создает дополнительную группу (EXTRA_GROUPS)
#                             с названием BFIN_INITCODE, которая используется
#                             для генерации .o файла, который передается параметром
#                             при генерации .ldr. Все параметры для сборки должны
#                             быть заданы аналогично работе с другими группами.
#                             В качестве исходного файла (SRC_BFIN_INITCODE) должен
#                             использоваться только один файл на .c только с одной
#                             функцией
#  Выходные файлы проекта (определяются после включения данного файла):
#    PROJECT_ELF           - .elf файл (абсолютный путь)
#    PROJECT_MAP           - .map файл (абсолютный путь)
#    PROJECT_BIN           - .bin файл (абсолютный путь)
#    PROJECT_HEX           - .hex файл (абсолютный путь)
#    PROJECT_LDR           - .ldr файл (абсолютный путь)
#
################################################################################

MKDIR ?= mkdir -p
RMDIR ?= rm -rf

#значения для компилятора и тулзов, если не переопределены, задаются как gcc с возможность
#префикса через TOOLCHAIN_PREFIX
CC   ?= $(TOOLCHAIN_PREFIX)gcc
CXX  ?= $(TOOLCHAIN_PREFIX)g++
CP   ?= $(TOOLCHAIN_PREFIX)objcopy
AS   ?= $(TOOLCHAIN_PREFIX)gcc -x assembler-with-cpp
SIZE ?= $(TOOLCHAIN_PREFIX)size
LDR  ?= $(TOOLCHAIN_PREFIX)ldr
HEX  ?= $(CP) -O ihex
BIN  ?= $(CP) -O binary





PROJECT_TARGET        ?= all
PROJECT_TARGET_CLEAN  ?= clean


PROJECT_SOURCE_DIR ?= $(abspath $(dir $(firstword $(MAKEFILE_LIST))))
PROJECT_BUILD_ROOT_DIR  ?= $(PROJECT_SOURCE_DIR)/build
ifdef PROJECT_BUILD_TYPE
PROJECT_BUILD_DIR  ?= $(PROJECT_BUILD_ROOT_DIR)/$(PROJECT_BUILD_TYPE)
else
PROJECT_BUILD_DIR  ?= $(PROJECT_BUILD_ROOT_DIR)
endif

PROJECT_OBJS_DIR   := $(PROJECT_BUILD_DIR)/objs


PROJECT_ELF_FILENAME ?= $(PROJECT_NAME).elf
PROJECT_ELF ?= $(PROJECT_BUILD_DIR)/$(PROJECT_ELF_FILENAME)
PROJECT_BIN = $(basename $(PROJECT_ELF)).bin
PROJECT_HEX = $(basename $(PROJECT_ELF)).hex
PROJECT_LDR = $(basename $(PROJECT_ELF)).ldr
PROJECT_MAP = $(basename $(PROJECT_ELF)).map





WARNOPTS ?= \
    -Werror=implicit-int -Werror=implicit-function-declaration -Werror=strict-prototypes -Werror=return-type \
    -Wall -Wextra \
    -Wformat-security -Winit-self -Wstrict-aliasing -Wfloat-equal \
    -Wundef -Wshadow -Wpointer-arith  \
    -Wwrite-strings  -Wsign-compare -Wlogical-op  -Winline \
    -Wno-error=inline -Wno-error=unused-variable -Wno-unused-parameter -Wno-error=unused-function -Wno-error=missing-field-initializers \
    -Werror=incompatible-pointer-types


# -Wframe-larger-than=400 -Wjump-misses-init -Wdouble-promotion





# если не используются группы исходников, то создаем одну группу с
# исходниками из  $(SRC)
ifeq ($(words $(BUILD_GROUPS)), 0)
    BUILD_GROUPS              = SINGLE_GROUP
    SRC_SINGLE_GROUP     := $(SRC)    
    #для совместимости со старыми версиями с отдельной переменной для исходников на ASM
    AS_SRC_SINGLE_GROUP  := $(AS_SRC)
endif

GEN_COMP_FLAGS = $(WARNOPTS)

GEN_COMP_FLAGS += -g -Wa,-ahlms=$$(@:.o=.lst)
# Generate dependency information
GEN_COMP_FLAGS += -MD -MP -MF $$(basename $$@).d

GEN_CFLAGS   = $(GEN_COMP_FLAGS) -Wstrict-prototypes
GEN_CPPFLAGS = $(GEN_COMP_FLAGS)
GEN_ASFLAGS  = $(GEN_COMP_FLAGS)
GEN_LDFLAGS = -Wl,-Map=$(PROJECT_MAP),--cref,--print-memory-usage $(LDFLAGS)

#если определено LDSCRIPT_TEMPLATE, то определяем переменные для генерации LD-файла
ifdef LDSCRIPT_TEMPLATE
    GEN_LDSCRIPT_DIR := $(PROJECT_BUILD_DIR)/ld
    GEN_LDSCRIPT = $(GEN_LDSCRIPT_DIR)/$(notdir $(LDSCRIPT_TEMPLATE))



    GEN_LDFLAGS += -T$(GEN_LDSCRIPT)

    LD_DEFS ?= $(DEFS)
    LD_INC_DIRS ?= $(INC_DIRS)
    LD_SYS_INC_DIRS ?= $(SYS_INC_DIRS)

    LD_DEFOPT := $(patsubst %,-D%,$(LD_DEFS))
    LD_INCOPT := $(patsubst %,-isystem%,$(abspath $(LD_SYS_INC_DIRS))) $(patsubst %,-I%,$(abspath $(LD_INC_DIRS)))

    # не до конца понятно значение -N опции, описание которой не найдено
    # с ней не собирается для PC, т.к. не может найти -lgcc_s, оставлена опция
    # только при задании шаблона LD, т.к. это используется в embeded
    GEN_LDFLAGS += -N
endif


# для bfin при генерации init-кода создаем для этого отдельную группу
ifdef BFIN_USE_INITCODE
    ifneq ($(BFIN_USE_INITCODE), 0)
        EXTRA_GROUPS += BFIN_INITCODE
        CFLAGS_BFIN_INITCODE += -fno-function-sections -fno-data-sections
        DEFS_BFIN_INITCODE += CHIP_INITCODE
    endif
else
    BFIN_USE_INITCODE := 0
endif




.DEFAULT_GOAL:=$(PROJECT_TARGET)
.PHONY: $(PROJECT_TARGET) $(PROJECT_TARGET_CLEAN)

define build_template =

GROUP_INC_DIRS     = $(INC_DIRS) $(INC_DIRS_$1)
GROUP_SYS_INC_DIRS = $(SYS_INC_DIRS) $(SYS_INC_DIRS_$1)
GROUP_DEFS             = $(DEFS) $(DEFS_$1)


GROUP_INCOPT_$1  := $$(patsubst %,-isystem%,$$(abspath $$(GROUP_SYS_INC_DIRS))) $$(patsubst %,-I%,$$(abspath $$(GROUP_INC_DIRS)))
GROUP_DEFOPT_$1  := $$(patsubst %,-D%,$$(GROUP_DEFS))


GROUP_SRC_C$1 := $$(filter %.c, $$(subst $$(PROJECT_SOURCE_DIR)/,,$$(foreach s,$$(SRC_$1),$$(abspath $$(s)))))
GROUP_OBJECTS_C$1 := $$(addprefix $$(PROJECT_OBJS_DIR)/,$$(GROUP_SRC_C$1:.c=.o))

GROUP_SRC_CPP$1 := $$(filter %.cpp, $$(subst $$(PROJECT_SOURCE_DIR)/,,$$(foreach s,$$(SRC_$1),$$(abspath $$(s)))))
GROUP_OBJECTS_CPP$1 := $$(addprefix $$(PROJECT_OBJS_DIR)/,$$(GROUP_SRC_CPP$1:.cpp=.o))

GROUP_SRC_CXX$1 := $$(filter %.cxx, $$(subst $$(PROJECT_SOURCE_DIR)/,,$$(foreach s,$$(SRC_$1),$$(abspath $$(s)))))
GROUP_OBJECTS_CXX$1 := $$(addprefix $$(PROJECT_OBJS_DIR)/,$$(GROUP_SRC_CXX$1:.cxx=.o))

GROUP_SRC_CC$1  := $$(filter %.cc, $$(subst $$(PROJECT_SOURCE_DIR)/,,$$(foreach s,$(SRC_$1),$$(abspath $$(s)))))
GROUP_OBJECTS_CC$1 := $$(addprefix $$(PROJECT_OBJS_DIR)/,$$(GROUP_SRC_CC$1:.cc=.o))


GROUP_SRC_A$1 := $$(filter %.s, $$(subst $$(PROJECT_SOURCE_DIR)/,,$$(foreach s,$$(SRC_$1),$$(abspath $$(s)))))
#для совместимости со старыми версиями с отдельной переменной для исходников на ASM
GROUP_SRC_A$1 += $$(filter %.s, $$(subst $$(PROJECT_SOURCE_DIR)/,,$$(foreach s,$$(AS_SRC_$1),$$(abspath $$(s)))))
GROUP_OBJECTS_A$1 := $$(addprefix $$(PROJECT_OBJS_DIR)/,$$(GROUP_SRC_A$1:.s=.o))

GROUP_SRC_AS$1 += $$(filter %.S, $$(subst $$(PROJECT_SOURCE_DIR)/,,$$(foreach s,$$(SRC_$1),$$(abspath $$(s)))))
GROUP_OBJECTS_AS$1 := $$(addprefix $$(PROJECT_OBJS_DIR)/,$$(GROUP_SRC_AS$1:.S=.o))


GROUP_OBJECTS_$1 := $$(GROUP_OBJECTS_C$1) $$(GROUP_OBJECTS_CPP$1) $$(GROUP_OBJECTS_CXX$1) $$(GROUP_OBJECTS_CC$1) $$(GROUP_OBJECTS_A$1) $$(GROUP_OBJECTS_AS$1)
PROJECT_OBJECTS += $$(GROUP_OBJECTS_$1)
PROJECT_CPP_OBJECTS += $$(GROUP_OBJECTS_CPP$1) $$(GROUP_OBJECTS_CXX$1) $$(GROUP_OBJECTS_CC$1)


ifneq ($2,0)
    PROJECT_ELF_OBJECTS += $$(GROUP_OBJECTS_$1)
else        
    PROJECT_EXTRA_OBJECTS += $$(GROUP_OBJECTS_$1)
endif



GROUP_CFLAGS_$1  = $(GEN_CFLAGS)  $(CFLAGS) $(CFLAGS_$1) $$(GROUP_INCOPT_$1) $$(GROUP_DEFOPT_$1)
GROUP_CPPFLAGS_$1  = $(GEN_CPPFLAGS)  $(CPPFLAGS) $(CXXFLAGS) $(CPPFLAGS_$1) $(CXXFLAGS_$1) $$(GROUP_INCOPT_$1) $$(GROUP_DEFOPT_$1)
GROUP_ASFLAGS_$1 = $(GEN_ASFLAGS) $(ASFLAGS) $(ASFLAGS_$1) $$(GROUP_INCOPT_$1) $$(GROUP_DEFOPT_$1)




$$(GROUP_OBJECTS_C$1): $(PROJECT_OBJS_DIR)/%.o: $(PROJECT_SOURCE_DIR)/%.c
	$(CC) $$(GROUP_CFLAGS_$1) -o $$@ -c $$<

$$(GROUP_OBJECTS_CPP$1): $(PROJECT_OBJS_DIR)/%.o: $(PROJECT_SOURCE_DIR)/%.cpp
	$(CXX) $$(GROUP_CPPFLAGS_$1) -o $$@ -c $$<
$$(GROUP_OBJECTS_CXX$1): $(PROJECT_OBJS_DIR)/%.o: $(PROJECT_SOURCE_DIR)/%.cxx
	$(CXX) $$(GROUP_CPPFLAGS_$1) -o $$@ -c $$<
$$(GROUP_OBJECTS_CC$1): $(PROJECT_OBJS_DIR)/%.o: $(PROJECT_SOURCE_DIR)/%.cc
	$(CXX) $$(GROUP_CPPFLAGS_$1) -o $$@ -c $$<

$$(GROUP_OBJECTS_A$1): $(PROJECT_OBJS_DIR)/%.o: $(PROJECT_SOURCE_DIR)/%.s
	$(AS) $$(GROUP_ASFLAGS_$1) -o $$@ -c $$<
$$(GROUP_OBJECTS_AS$1): $(PROJECT_OBJS_DIR)/%.o: $(PROJECT_SOURCE_DIR)/%.S
	 $(AS) $$(GROUP_ASFLAGS_$1) -o $$@ -c $$<
endef

$(foreach build_group,$(EXTRA_GROUPS),$(eval $(call build_template,$(build_group),0)))
$(foreach build_group,$(BUILD_GROUPS),$(eval $(call build_template,$(build_group),1)))

# если нет исходников на C++, то используем для создания elf компилятор C, иначе - C++
ifeq ($(words $(PROJECT_CPP_OBJECTS)), 0)
    CC_ELF = $(CC)
else
    CC_ELF = $(CXX)
endif

ifneq ($(BFIN_USE_INITCODE), 0)
    #добавляем в флаги при создании .ldr и в зависимости
    LDRCREATEFLAGS += -i $(GROUP_OBJECTS_BFIN_INITCODE)
    LDRDEPS += $(GROUP_OBJECTS_BFIN_INITCODE)
endif

PROJECT_OBJECTS_DIRS :=$(sort $(foreach f,$(PROJECT_OBJECTS),$(dir $(f))))

# для совместимости с ранее используемой переменной LIBDIRS
ifdef LIBDIRS
    LIB_DIRS += LIBDIRS
endif

LIBOPT  := $(patsubst %,-L%,$(LIB_DIRS)) $(patsubst %,-l%,$(LIBS))


$(PROJECT_TARGET): $(PROJECT_OBJECTS) $(PROJECT_ELF)
	$(SIZE) -A $(PROJECT_ELF)

$(PROJECT_OBJECTS): | $(PROJECT_OBJECTS_DIRS)

$(PROJECT_OBJECTS), $(PROJECT_ELF) $(GEN_LDSCRIPT): $(MAKEFILE_LIST) $(PROJECT_PREBUILD_DEPS)

$(PROJECT_BUILD_DIR):
	$(MKDIR) $@

$(GEN_LDSCRIPT_DIR)  : | $(PROJECT_BUILD_DIR)
	$(MKDIR) $@

$(GEN_LDSCRIPT): $(LDSCRIPT_TEMPLATE) | $(GEN_LDSCRIPT_DIR)
	$(CC) -E -undef -P -CC $(LD_DEFOPT) $(LD_INCOPT) $< -o $@


# Recipe to create the output object file directories.
$(PROJECT_OBJECTS_DIRS) : | $(PROJECT_BUILD_DIR)
	$(MKDIR) $@

$(PROJECT_LDR): $(PROJECT_ELF) $(LDRDEPS) $(LDREXTRAELFS)
	$(LDR) $(LDRFLAGS) -c $(LDRCREATEFLAGS) $@  $< $(LDREXTRAELFS)


$(PROJECT_ELF): $(PROJECT_ELF_OBJECTS) $(GEN_LDSCRIPT) | $(LDREXTRATARGETS)
	$(CC_ELF) $(PROJECT_ELF_OBJECTS) $(GEN_LDFLAGS) $(LIBOPT) -o $@

${PROJECT_HEX}: $(PROJECT_ELF)
	$(HEX) $< $@

$(PROJECT_BIN): $(PROJECT_ELF)
	$(BIN) $< $@

$(PROJECT_TARGET_CLEAN):
	$(RMDIR) $(PROJECT_BUILD_DIR)


# Include dependency files.
-include $(PROJECT_OBJECTS:.o=.d)
